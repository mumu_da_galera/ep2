package ep2;
import java.util.ArrayList;
import java.util.List;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import static java.awt.FlowLayout.LEFT;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.util.Vector;


public  class FrotaSwing extends JFrame{
   
    Vector arrayCarreta;
    Vector arrayVan;
    Vector arrayCarro;
    Vector arrayMoto;
    
    FrotaSwing(){
        arrayCarreta = new Vector();
            Carreta carretaVeiculo = new Carreta();
                    arrayCarreta.add(carretaVeiculo);
                                                     
        arrayVan = new Vector();
            Van vanVeiculo = new Van();
                            arrayVan.add(vanVeiculo);
        arrayCarro = new Vector();
            Carro carroVeiculo = new Carro();
                            arrayCarro.add(carroVeiculo);
        arrayMoto = new Vector();
            Moto motoVeiculo = new Moto();
                            arrayMoto.add(motoVeiculo);
        relatorio();        
    }
 
    private void relatorio(){
        
            setTitle("Relatorio da frota");
            setLayout(new GridLayout(4,1,2,2));
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            setSize(530,260);
            setVisible(true);
 ////////////////////////////////////////////////////////////////////////////
                JPanel painelcarreta=new JPanel();
                painelcarreta.setLayout(new FlowLayout(LEFT));
                
                JLabel labelCarreta = new JLabel("Carreta");
                painelcarreta.add(labelCarreta);
                
                JButton maisCarreta = new JButton("+");
                    maisCarreta.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent somarcarreta) {
                            Carreta carretaVeiculo = new Carreta();
                            arrayCarreta.add(carretaVeiculo);
                            }
                    });
                
                JButton menosCarreta = new JButton("-");
                    menosCarreta.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent subtraircarreta) {   
                            Veiculo carretaVeiculo = new Veiculo();
                            arrayCarreta.remove(carretaVeiculo);
                            }
                    });
              
                painelcarreta.add(maisCarreta);
                painelcarreta.add(menosCarreta);
                
                JLabel numeroCarreta = new JLabel(""+ arrayCarreta.size());
                painelcarreta.add(numeroCarreta);
                             
            add(painelcarreta);
 ///////////////////////////////////////////////////////////////////////////////////
                JPanel painelvan=new JPanel();
                painelvan.setLayout(new FlowLayout(LEFT));
                
                JLabel labelvan = new JLabel("Van");
                painelvan.add(labelvan);
                
                JButton maisvan = new JButton("+");
                    maisvan.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent somarvan) {                            
                           Van vanVeiculo = new Van();
                            arrayVan.add(vanVeiculo);
                            }
                    });
                
                JButton menosvan = new JButton("-");
                   menosvan.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent subtrairvan) {                           
                            Veiculo vanVeiculo = new Veiculo();
                           arrayVan.remove(vanVeiculo);
                            }
                    });
              
                painelvan.add(maisvan);
                painelvan.add(menosvan);
                
                
                JLabel numeroVan = new JLabel(""+ arrayVan.size());
                painelvan.add(numeroVan);
                             
            add(painelvan);
 ///////////////////////////////////////////////////////////////////////////////////
                JPanel painelCarro=new JPanel();
                painelCarro.setLayout(new FlowLayout(LEFT));
                
                JLabel labelCarro = new JLabel("Carro");
                painelCarro.add(labelCarro);
                
                JButton maisCarro = new JButton("+");
                    maisCarro.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent somarCarro) {                            
                            Carro carroVeiculo = new Carro();
                            arrayCarro.add(carroVeiculo);
                            }
                    });
                
                JButton menosCarro = new JButton("-");
                   menosCarro.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent subtraircarro) {
                            Veiculo carroVeiculo = new Veiculo();
                            arrayCarro.remove(carroVeiculo);
                            }
                    });
              
                painelCarro.add(maisCarro);
                painelCarro.add(menosCarro);
                
                
                JLabel numeroCarro = new JLabel(""+ arrayCarro.size());
                painelCarro.add(numeroCarro);
                             
            add(painelCarro);
 /////////////////////////////////////////////////////////////////////////////////
                JPanel painelMoto=new JPanel();
                painelMoto.setLayout(new FlowLayout(LEFT));
                
                JLabel labelMoto = new JLabel("Moto");
                painelMoto.add(labelMoto);
                
                JButton maisMoto = new JButton("+");
                    maisMoto.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent somarMoto) {                            
                            Moto motoVeiculo = new Moto();
                            arrayMoto.add(motoVeiculo);
                            }
                    });
                
                JButton menosMoto = new JButton("-");
                   menosMoto.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent subtrairmoto) {                           
                            Moto motoVeiculo = new Moto();
                            arrayCarro.remove(motoVeiculo);
                            }
                    });
              
                painelMoto.add(maisMoto);
                painelMoto.add(menosMoto);
                
                
                JLabel numeroMoto = new JLabel(""+ arrayMoto.size());
                painelMoto.add(numeroMoto);
                             
            add(painelMoto);
    }
    
}



