package ep2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import static java.awt.FlowLayout.LEFT;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.lang.String;


public class FreteSwing extends JFrame  {
    public  FreteSwing(){
        super("Novo Frete");
        levantamento();        
    }

    private JTextField lucroField;
    String lucroTexto;
    private JTextField pesoField;
    String pesoTexto= new String();
    private JTextField distanciaField;
    String distanciaTexto;
    private JTextField tempoField;
    String tempoTexto;
    
    private void levantamento(){
        setLayout(new GridLayout(5,1,2,2));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(530,260);
        setVisible(true);
        
            JPanel painel1=new JPanel();
                painel1.setLayout(new FlowLayout(LEFT));                
                JLabel lucro=new JLabel("Lucro");
                lucroField=new JTextField(20);
                painel1.add(lucro);                
                painel1.add(lucroField);
                
        add(painel1);    
        
            JPanel painel2=new JPanel();
                painel2.setLayout(new FlowLayout(LEFT));                
                JLabel peso =new JLabel("Peso");
                pesoField=new JTextField(20);
                painel2.add(peso);
                painel2.add(pesoField);
        add(painel2);
        
            JPanel painel3=new JPanel();
                painel3.setLayout(new FlowLayout(LEFT));                
                JLabel distancia=new JLabel("Distancia");
                distanciaField=new JTextField(20);
                painel3.add(distancia);
                painel3.add(distanciaField);
        add(painel3); 
        
            JPanel painel4=new JPanel();
                painel4.setLayout(new FlowLayout(LEFT));                
                JLabel tempo=new JLabel("Tempo");
                tempoField=new JTextField(20);
                painel4.add(tempo);
                painel4.add(tempoField);
        add(painel4); 
            
            JPanel painel5=new JPanel();
                painel5.setLayout(new FlowLayout(LEFT));
            JButton botaoconfirmar = new JButton("confirmar");
                botaoconfirmar.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent confirmarfrete) {                            
                            lucroTexto=lucroField.getText();
                            pesoTexto=pesoField.getText();
                            distanciaTexto=distanciaField.getText();
                            tempoTexto=tempoField.getText();
                            new Calculo( lucroTexto,pesoTexto,distanciaTexto,tempoTexto);
                            
                            
                            
                            }
                    });
                
            JButton botaocancelar = new JButton("cancelar");
                botaocancelar.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent cancelarfrete) {                            
                            lucroField.setText("");
                            pesoField.setText("");
                            distanciaField.setText("");
                            tempoField.setText("");
                            
                            }
                    });
            
            painel5.add(botaoconfirmar);
            painel5.add(botaocancelar);
        add(painel5);
            
    }
}
