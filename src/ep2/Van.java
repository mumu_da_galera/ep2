package ep2;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class Van extends Veiculo {
    
	public void Van(){
        this.combustivel="Diesel";

        this.rendimento=10;
    
        this.cargaMaxima=3500;
    
        this.velMed=80;
    
        this.reducao=0.001f;
        
    }

    public float preco(float lucro,float peso,float distancia,float tempo){
        float perda= this.reducao*peso;
        float rendimentoreal=this.rendimento - perda;
        float custo= (distancia/rendimentoreal)*  3.869f ;
        float preco= custo/(1-lucro);
        
        return preco;
    }
}