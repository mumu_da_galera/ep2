package ep2;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class Carro extends Veiculo {
    public void Carro(){
           
        this.combustivel="Gasolina";

        this.rendimento=14;
    
        this.cargaMaxima=360;
    
        this.velMed=100;
    
        this.reducao=0.025f;
        }
            
        
    public float preco(float lucro,float peso,float distancia,float tempo){
        float perda= this.reducao*peso;
        float rendimentoreal=this.rendimento - perda;
        float custo= (distancia/rendimentoreal)*  4.449f ;
        float preco= custo/(1-lucro);
        
        return preco;
    }

   
}