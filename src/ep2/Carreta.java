package ep2;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class Carreta extends Veiculo {
    Vector carretaVector=new Vector();
    
    Carreta(){
        this.combustivel="Diesel";

        this.rendimento=8;
    
        this.cargaMaxima=30000;
    
        this.velMed=60;
    
        this.reducao=0.0002f;
        
    }
    
    public float preco(float lucro,float peso,float distancia,float tempo){
        float perda= this.reducao*peso;
        float rendimentoreal=this.rendimento - perda;
        float custo= (distancia/rendimentoreal)* 3.869f ;
        float preco= custo/(1-lucro);
        
        return preco;
    }
    
}