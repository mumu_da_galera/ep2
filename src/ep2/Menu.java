package ep2;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;


public class Menu extends JFrame{

    public Menu(){
        escolha();
        
    }

    
    public void escolha(){
        setLayout(new GridLayout(2,1,2,2));
        
            JButton botaoFrota = new JButton("Frota");
                botaoFrota.setBounds(100,100,10,60);
                botaoFrota.addActionListener(new ActionListener() {
                            public void actionPerformed(ActionEvent abrirFrota) {                            
                                new FrotaSwing();
                                }
                        });
            JButton botaoFrete = new JButton ("Frete");
                botaoFrete.setBounds(100,100,10,60);
               botaoFrete.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent abrirFrete) {                            
                                 new FreteSwing();
                            }
                    });
            
        add(botaoFrota);
        add(botaoFrete);
    }
}
