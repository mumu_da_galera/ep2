package ep2;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class Moto extends Veiculo {
    
	public void Moto(){
        this.combustivel="Gasolina";

        this.rendimento=50;
    
        this.cargaMaxima=50;
    
        this.velMed=10;
    
        this.reducao=0.0002f;
        
    }
      
        public float preco(float lucro,float peso,float distancia,float tempo){
        float perda= this.reducao*peso;
        float rendimentoreal=this.rendimento - perda;
        float custo= (distancia/rendimentoreal)*  4.449f ;
        float preco= custo/(1-lucro);
        
        return preco;
    }
}